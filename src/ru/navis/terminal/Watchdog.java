package ru.navis.terminal;

import android.util.Log;

public class Watchdog extends Thread {
    private int counter = 0;
    private long period;
    private Runnable onTrigger;

    private Object lock = new Object();
    private boolean running = true;

    /// If clear() is not called for time longer than period, than onTrigger would be called
    Watchdog(long period, Runnable onTrigger) {
        this.period = period;
        this.onTrigger = onTrigger;
    }

    public void run() {
        while (running) {
            try {
                synchronized (lock) {
                    lock.wait(period);
                }
            } catch (InterruptedException e) {
                continue;
            }

            ++counter;

            if (counter == 2) {
                running = false;

                Log.i("agro", "Watchdog triggered");
                onTrigger.run();
                return;
            }
        }
    }

    void clear() {
        counter = 0;
    }

    void cancel() {
        synchronized (lock) {
            running = false;
            lock.notify();
        }
    }
}
